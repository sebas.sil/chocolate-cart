import { useEffect, useState } from 'react'
import style from './App.module.css'
import { Item } from './component/Cart/Item'
import { Summary } from './component/Cart/Summary'
import { Header, State } from './component/Cart/Header'
import { getProducts, Product } from './service/ItemService'

const App = () => {

  const [products, setProducts] = useState<Product[]>([])
  const [total, setTotal] = useState<number>(0)
  const [state, setState] = useState<State>('acima')

  useEffect(() => {
    getProducts(state).then(e => {
      setProducts(e.items)
      setTotal(e.value)
    })
  }, [state])

  function handleTogleState():State {
    return state == 'acima' ? 'abaixo' : 'acima'
  }

  return (
    <div className={style.content}>
      <Header setState={setState} handleTogleState={handleTogleState} />
      <div>
        {products.map(p =>
          <Item key={p.productId} photo={p.imageUrl} price={p.price} sellingPrice={p.sellingPrice} title={p.name} />
        )}
      </div>
      <Summary total={total} hasFreeShipping={total > 1000} />
      <div className={style.footer}>
        <button type='button'>Finalizar compra</button>
      </div>
    </div>
  )
}

export type { State }
export { App }