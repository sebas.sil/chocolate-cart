import style from './Header.module.css'

type State = 'acima'|'abaixo'

type HeaderProps = {
    setState: (state: State) => void
    handleTogleState: () => State
}

const Header = ({ setState, handleTogleState } : HeaderProps) => {
    return (
      <div className={style.header}>
        <p>Meu carrinho</p>
        <span role='button' className={style.change} onClick={e => setState(handleTogleState())}>{handleTogleState()}</span>
      </div>
    )
}

export type { State }
export { Header }