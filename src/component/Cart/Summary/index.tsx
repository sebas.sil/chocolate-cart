import { formatter } from '../../../service/Utils'
import style from './Summary.module.css'

type SumaryProps = {
  total: number
  hasFreeShipping: boolean
}

const Summary = ({ total, hasFreeShipping }: SumaryProps) => {
    return (
      <div className={style.summaryContent}>
          <div className={style.summary}>
            <span>Total</span>
            <span>{formatter.format(total/100)}</span>
          </div>
          {hasFreeShipping && <div className={style.congrats}><p>Parabéns, sua compra tem frete grátis!</p></div>}
        </div>
    )
}

export { Summary }