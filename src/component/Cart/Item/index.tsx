import style from './Item.module.css'
import { formatter } from '../../../service/Utils'

type ItemProps = {
    photo:string
    title:string
    price:number
    sellingPrice:number
}

const Item = ({photo, title, price, sellingPrice}: ItemProps) => {

    return (
        <div className={style.content}>
            <div className={style.photo}>
                <img src={photo} />
            </div>
            <div className={style.info}>
                <span className={style.title}>{title}</span>
                <span className={style.price}>{formatter.format(price/100)}</span>
                <span className={style.sellingPrice}>{formatter.format(sellingPrice/100)}</span>
            </div>
        </div>
    )
}

export { Item }