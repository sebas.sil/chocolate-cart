import { State } from '../component/Cart/Header/index'

type Product = {
    productId:number
    name:string
    price:number
    sellingPrice:number
    imageUrl:string
}

type ItemResponse = {
    items:Product[]
    value:number
}

async function getProducts(type:State = 'abaixo'):Promise<ItemResponse> {
    return fetch(type + '-10-reais.json').then(res => res.json() as Promise<ItemResponse>)
}

export type { Product }
export { getProducts }