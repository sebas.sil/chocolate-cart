import React from 'react'
import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { Header } from '../../../src/component/Cart/Header'
import { State } from '../../../src/App'

describe('test suit to Header component', () => {
    it('should render the cart head component', () => {
        const mockHandle = jest.fn(() => 'acima' as State)
        const mockSetState = jest.fn()

        render(<Header handleTogleState={mockHandle} setState={mockSetState}/>)
        expect(screen.getByText('Meu carrinho')).toBeTruthy()
        expect(screen.getByRole('button', {name:'acima'})).toBeTruthy()
        expect(mockHandle.mock.calls.length).toBe(1);
        expect(mockSetState.mock.calls.length).toBe(0);
    })

    it('shohuld change the button label and action the function', async () => {
        const mockHandle = jest.fn(() => 'acima' as State)
        const mockSetState = jest.fn()

        render(<Header handleTogleState={mockHandle} setState={mockSetState}/>)

        const button = screen.getByText('acima')
        await userEvent.click(button)

        expect(screen.getByText('Meu carrinho')).toBeTruthy()
        expect(screen.getByRole('button', {name:'acima'})).toBeTruthy()
        expect(mockHandle.mock.calls.length).toBe(2);
        expect(mockSetState.mock.calls.length).toBe(1);
    })
})