import React from 'react'
import { render, screen } from '@testing-library/react'
import { Item } from '../../../src/component/Cart/Item'

describe('test suit to Item component', () => {
    it('should render the cart item component', () => {
        render(<Item photo='foto_link' price={199} sellingPrice={59} title={'teste_item'} />)
        expect(screen.getByRole('img')?.getAttribute('src')).toBe('foto_link')
        expect(screen.getByText('R$ 1,99')).toBeTruthy()
        expect(screen.getByText('R$ 0,59')).toBeTruthy()
        expect(screen.getByText('teste_item')).toBeTruthy()
    })
})