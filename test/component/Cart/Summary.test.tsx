import React from 'react'
import { render, screen } from '@testing-library/react'
import { Summary } from '../../../src/component/Cart/Summary'

describe('test suit to Summary component', () => {
    it('should render the cart summary component with free shipping', () => {
        render(<Summary hasFreeShipping={true} total={100} />)
        expect(screen.getByText('Total')).toBeTruthy()
        expect(screen.getByText('R$ 1,00')).toBeTruthy()
        expect(screen.getByText('Parabéns, sua compra tem frete grátis!')).toBeTruthy()
    })

    it('should render the cart summary component without free shipping', () => {
        render(<Summary hasFreeShipping={false} total={100} />)
        expect(screen.getByText('Total')).toBeTruthy()
        expect(screen.getByText('R$ 1,00')).toBeTruthy()
        expect(screen.queryByText('Parabéns, sua compra tem frete grátis!')).toBeFalsy()
    })
})