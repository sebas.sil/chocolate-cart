import React from 'react'
import { act, render, screen, waitFor, waitForElementToBeRemoved } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { App } from '../src/App'

global.fetch = jest.fn((url:string) => Promise.resolve({json: () => Promise.resolve(require('../public/' + url))})) as jest.Mock
global.IS_REACT_ACT_ENVIRONMENT = true

describe('test suit to App component', () => {

    it('should render the app component with free shipping', async () => {

        render(<App />)
        await waitFor(() => expect(global.fetch).toHaveReturned())
        expect((global.fetch as jest.Mock).mock.calls.length).toBe(1)
        expect((global.fetch as jest.Mock).mock.calls[0][0]).toBe('acima-10-reais.json')
        expect(screen.findByText('abaixo')).toBeTruthy()
        expect(screen.findByText('Meu Carrinho')).toBeTruthy()
        expect(screen.findByText('Parabéns, sua compra tem frete grátis!')).toBeTruthy()
        expect(screen.findByText('Finalizar Compra')).toBeTruthy()
        expect(screen.findByText('TRUFA MORANGO E MARACUJÁ 13,5 G')).toBeTruthy()

        await act(() => global.fetch as unknown as Promise<any>)
    })

    it('should render the app component without free shipping', async () => {

        render(<App />)
        const button = screen.getByText('abaixo')
        await userEvent.click(button)
        await act(() => global.fetch as unknown as Promise<any>)
        
        await waitFor(() => expect(global.fetch).toHaveReturnedTimes(2))

        expect((global.fetch as jest.Mock).mock.calls.length).toBe(2)
        expect((global.fetch as jest.Mock).mock.calls[1][0]).toBe('abaixo-10-reais.json')

        expect(screen.getByRole('button', {name: /acima/i})).toBeTruthy()
        expect(screen.findByText('Meu Carrinho')).toBeTruthy()
        
        expect(screen.queryByText('<p>Parabéns, sua compra tem frete grátis!</p>')).toBeFalsy()
        expect(screen.getByRole('button', {name: /Finalizar Compra/i})).toBeTruthy()
        expect(screen.queryByText('TRUFA MORANGO E MARACUJÁ 13,5 G')).toBeFalsy()


    })
})