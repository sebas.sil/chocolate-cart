[![coverage report](https://gitlab.com/sebas.sil/chocolate-cart/badges/main/coverage.svg)](https://gitlab.com/sebas.sil/chocolate-cart/-/commits/main)

Basic React Test

## Layout

basic [layout](https://www.figma.com/file/4dCjhbUZt9h9zMY0Fmo3fI/chocolate-cart) created in figma, without caring about the realization of values ​​or spacing. This layout was created to guide the construction of the code.

![List with free shipping](/public/img1.png "free shipping")

![List without free shipping](/public/img2.png "with shipping")

## Deploy

You can deploy it on your own Vercel account [![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/git/external?repository-url=https://gitlab.com/sebas.sil/chocolate-cart&project-name=chocolate-cart&repository-name=moveit). Or click on [chocolate-cart](https://chocolate-cart.vercel.app/) to see it running.
